# translation of gail to Bosnian
# This file is distributed under the same license as the gail package.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER.
# Kenan Hadžiavdić <kenanh@frisurf.no>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: gail.HEAD\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-03-10 15:53+0800\n"
"PO-Revision-Date: 2004-03-03 16:47+0000\n"
"Last-Translator: Kenan Hadžiavdić <kenanh@frisurf.no>\n"
"Language-Team: Bosnian <lokal@lugbih.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0.2\n"

#: gail/gailimage.c:55
msgid "dialog authentication"
msgstr "dijalog provjera autentičnosti"

#: gail/gailimage.c:56
msgid "dialog information"
msgstr "dijalog informacije"

#: gail/gailimage.c:57
msgid "dialog warning"
msgstr "dijalog upozorenje"

#: gail/gailimage.c:58
msgid "dialog error"
msgstr "dijalog greška"

#: gail/gailimage.c:59
msgid "dialog question"
msgstr "dijalog pitanje"

#: gail/gailimage.c:60
msgid "drag and drop"
msgstr "puvuci i ispusti"

#: gail/gailimage.c:61
msgid "multiple drag and drop"
msgstr "višestruki povuci i ispusti"

#: gail/gailimage.c:62
msgid "add"
msgstr "dodaj"

#: gail/gailimage.c:63
msgid "apply"
msgstr "primijeni"

#: gail/gailimage.c:64
msgid "bold"
msgstr "masno"

#: gail/gailimage.c:65
msgid "cancel"
msgstr "odustani"

#: gail/gailimage.c:66
msgid "cdrom"
msgstr "cdrom"

#: gail/gailimage.c:67
msgid "clear"
msgstr "očisti"

#: gail/gailimage.c:68
msgid "close"
msgstr "zatvori"

#: gail/gailimage.c:69
msgid "color picker"
msgstr "birač boja"

#: gail/gailimage.c:70
msgid "convert"
msgstr "konvertiraj"

#: gail/gailimage.c:71
msgid "copy"
msgstr "kopiraj"

#: gail/gailimage.c:72
msgid "cut"
msgstr "izreži"

#: gail/gailimage.c:73
msgid "delete"
msgstr "obriši"

#: gail/gailimage.c:74
msgid "execute"
msgstr "izvrši"

#: gail/gailimage.c:75
msgid "find"
msgstr "pronađi"

#: gail/gailimage.c:76
msgid "find and replace"
msgstr "pronađi i zamijeni"

#: gail/gailimage.c:77
msgid "floppy"
msgstr "disketa"

#: gail/gailimage.c:78
msgid "go to bottom"
msgstr "idi na dno"

#: gail/gailimage.c:79
msgid "go to first"
msgstr "idi na prvo"

#: gail/gailimage.c:80
msgid "go to last"
msgstr "idi na zadnje"

#: gail/gailimage.c:81
msgid "go to top"
msgstr "idi na vrh"

#: gail/gailimage.c:82
msgid "go back"
msgstr "idi nazad"

#: gail/gailimage.c:83
msgid "go down"
msgstr "idi dolje"

#: gail/gailimage.c:84
msgid "go forward"
msgstr "idi naprijed"

#: gail/gailimage.c:85
msgid "go up"
msgstr "idi gore"

#: gail/gailimage.c:86
msgid "help"
msgstr "pomoć"

#: gail/gailimage.c:87
msgid "home"
msgstr "početak"

#: gail/gailimage.c:88
msgid "index"
msgstr "indeks"

#: gail/gailimage.c:89
msgid "italic"
msgstr "koso"

#: gail/gailimage.c:90
msgid "jump to"
msgstr "skoči na"

#: gail/gailimage.c:91
msgid "justify center"
msgstr "poravnaj centar"

#: gail/gailimage.c:92
msgid "justify fill"
msgstr "poravnaj popunjeno"

#: gail/gailimage.c:93
msgid "justify left"
msgstr "poravnaj lijevo"

#: gail/gailimage.c:94
msgid "justify right"
msgstr "poravnaj desno"

#: gail/gailimage.c:95
msgid "missing image"
msgstr "nedostaje slika"

#: gail/gailimage.c:96
msgid "new"
msgstr "novi"

#: gail/gailimage.c:97
msgid "no"
msgstr "ne"

#: gail/gailimage.c:98
msgid "ok"
msgstr "uredu"

#: gail/gailimage.c:99
msgid "open"
msgstr "otvori"

#: gail/gailimage.c:100
msgid "paste"
msgstr "umetni"

#: gail/gailimage.c:101
msgid "preferences"
msgstr "opcije"

#: gail/gailimage.c:102
msgid "print"
msgstr "štampaj"

#: gail/gailimage.c:103
msgid "print preview"
msgstr "pregled prije štampanja"

#: gail/gailimage.c:104
msgid "properties"
msgstr "osobine"

#: gail/gailimage.c:105
msgid "quit"
msgstr "izađi"

#: gail/gailimage.c:106
msgid "redo"
msgstr "ponovi"

#: gail/gailimage.c:107
msgid "refresh"
msgstr "osvježi"

#: gail/gailimage.c:108
msgid "remove"
msgstr "ukloni"

#: gail/gailimage.c:109
msgid "revert to saved"
msgstr "vrati snimljeno"

#: gail/gailimage.c:110
msgid "save"
msgstr "snimi"

#: gail/gailimage.c:111
msgid "save as"
msgstr "snimi kao"

#: gail/gailimage.c:112
msgid "select color"
msgstr "izaberi boju"

#: gail/gailimage.c:113
msgid "select font"
msgstr "izaberi font"

#: gail/gailimage.c:114
msgid "sort ascending"
msgstr "sortiraj rastuće"

#: gail/gailimage.c:115
msgid "sort descending"
msgstr "sortiraj opadajuće"

#: gail/gailimage.c:116
msgid "spell check"
msgstr "provjera pravopisa"

#: gail/gailimage.c:117
msgid "stop"
msgstr "zaustavi"

#: gail/gailimage.c:118
msgid "strikethrough"
msgstr "prekriži"

#: gail/gailimage.c:119
msgid "undelete"
msgstr "povrati izbrisano"

#: gail/gailimage.c:120
msgid "underline"
msgstr "podvuci"

#: gail/gailimage.c:121
msgid "yes"
msgstr "da"

#: gail/gailimage.c:122
msgid "zoom 100 percent"
msgstr "normalna veličina"

#: gail/gailimage.c:123
msgid "zoom fit"
msgstr "podesi veličinu"

#: gail/gailimage.c:124
msgid "zoom in"
msgstr "uvećaj"

#: gail/gailimage.c:125
msgid "zoom out"
msgstr "umanji"

#: gail/gailimage.c:127
msgid "timer"
msgstr "štoperica"

#: gail/gailimage.c:128
msgid "timer stop"
msgstr "zaustavljena štoperica"

#: gail/gailimage.c:129
msgid "trash"
msgstr "smeće"

#: gail/gailimage.c:130
msgid "trash full"
msgstr "puno smeće"

#: gail/gailimage.c:131
msgid "scores"
msgstr "rezultati"

#: gail/gailimage.c:132
msgid "about"
msgstr "o"

#: gail/gailimage.c:133
msgid "blank"
msgstr "prazno"

#: gail/gailimage.c:134
msgid "volume"
msgstr "jačina zvuka"

#: gail/gailimage.c:135
msgid "midi"
msgstr "midi"

#: gail/gailimage.c:136
msgid "microphone"
msgstr "mikrofon"

#: gail/gailimage.c:137
msgid "line in"
msgstr "ulazna linija"

#: gail/gailimage.c:138
msgid "mail"
msgstr "pošta"

#: gail/gailimage.c:139
msgid "mail receive"
msgstr "primanje pošte"

#: gail/gailimage.c:140
msgid "mail send"
msgstr "pošalji email"

#: gail/gailimage.c:141
msgid "mail reply"
msgstr "email odgovor"

#: gail/gailimage.c:142
msgid "mail forward"
msgstr "proslijeđivanje pošte"

#: gail/gailimage.c:143
msgid "mail new"
msgstr "novi email"

#: gail/gailimage.c:144
msgid "attach"
msgstr "priloži"

#: gail/gailimage.c:145
msgid "book red"
msgstr "crvena knjiga"

#: gail/gailimage.c:146
msgid "book green"
msgstr "zelena knjiga"

#: gail/gailimage.c:147
msgid "book blue"
msgstr "plava knjiga"

#: gail/gailimage.c:148
msgid "book yellow"
msgstr "žuta knjiga"

#: gail/gailimage.c:149
msgid "book open"
msgstr "otvorena knjiga"

#: gail/gailimage.c:150
msgid "multiple file"
msgstr "višestruka datoteka"

#: gail/gailimage.c:151
msgid "not"
msgstr "ne"

#: gail/gailimage.c:152
msgid "table borders"
msgstr "granice tablice"

#: gail/gailimage.c:153
msgid "table fill"
msgstr "popunjavanje tablice"

#: gail/gailimage.c:154
msgid "text indent"
msgstr "uvuci tekst"

#: gail/gailimage.c:155
msgid "text unindent"
msgstr "izvuci tekst"

#: gail/gailimage.c:156
msgid "text bulleted list"
msgstr "tekst lista pod tačkama"

#: gail/gailimage.c:157
msgid "text numbered list"
msgstr "tekst lista pod brojevima"

#: gail/gailimage.c:158
msgid "authentication"
msgstr "provjera autentičnosti"
